const mysql = require('mysql');

// Configuración de la conexión a la base de datos
const db = mysql.createConnection({
  host: 'db', // Cambia esto si tu base de datos se encuentra en otro host
  user: 'root', // Cambia esto si tu usuario de MySQL es diferente
  password: 'Javier10', // Cambia esto por tu contraseña de MySQL
  database: 'Fulbito' // Reemplaza esto por el nombre real de tu base de datos
});

// Conexión a la base de datos
db.connect((err) => {
  if (err) {
    console.error('Error al conectar a la base de datos:', err);
  } else {
    console.log('Conexión exitosa a la base de datos');
  }
});

// Función genérica para ejecutar consultas SQL
function executeQuery(sql, params, callback) {
  db.query(sql, params, (err, result) => {
    if (err) {
      console.error('Error al ejecutar la consulta:', err);
      callback(err, null);
    } else {
      callback(null, result);
    }
  });
}

// Función específica para obtener los datos de la tabla "Jugador"
function obtenerJugadores(callback) {
  const sql = 'SELECT JugadorID, Nombre, Apellido, Apodo, SuspensionesTotales, PartidosJugados, Valoracion FROM Jugador ORDER BY Valoracion DESC;';
  executeQuery(sql, [], callback);
}

// Función específica para obtener los datos de la tabla "Jugador"
function obtenerJugadoresyAvatares(callback) {
  const sql = 'SELECT JugadorID, Nombre, Apellido, Apodo, SuspensionesTotales, PartidosJugados, Valoracion, RecetaHTML FROM Jugador NATURAL JOIN Avatar ORDER BY Nombre;';
  executeQuery(sql, [], callback);
}

// Función para obtener los datos de la tabla "Jugador" según un array de JugadorID
function obtenerJugadoresPartido(jugadorIDs, callback) {
  console.log("Entré a la función de búsqueda en BD");

  // Crear una cadena de marcadores de posición '?' para cada JugadorID
  const placeholders = jugadorIDs.map(() => '?').join(', ');

  // Construir la consulta SQL dinámica
  const sql = `SELECT JugadorID, Nombre, Apellido, Apodo, (Valoracion * 80) AS Valoracion, RecetaHTML
               FROM Jugador NATURAL JOIN Avatar
               WHERE JugadorID IN (${placeholders})
               ORDER BY Valoracion DESC;`;

  // Ejecutar la consulta con los valores de JugadorID
  executeQuery(sql, jugadorIDs, callback);
}

// Función específica para obtener los datos de la tabla "Jugador" y "Avatar"
function obtenerJugador(JugadorID, callback) {
  const sql = 'SELECT JugadorID, Nombre, Apellido, Apodo, SuspensionesTotales, PartidosJugados, Valoracion, RecetaHTML FROM Jugador NATURAL JOIN Avatar WHERE JugadorID = ?;';
  executeQuery(sql, [JugadorID], callback);
}

// Función específica para obtener los datos de la tabla "Partido"
function obtenerPartidos(callback) {
  const sql = 'SELECT PartidoID, DATE_FORMAT(FechaPartido, "%d/%m/%Y") AS FechaPartido, GolesEquipo1, GolesEquipo2, PremioEquipo1, PremioEquipo2 FROM Partido ORDER BY PartidoID DESC;';
  executeQuery(sql, [], callback);
}

// Función específica para obtener los datos de la tabla "Partido" y "EquipoPartido"
function obtenerPartido(PartidoID, callback) {
  const sql = 'SELECT DATE_FORMAT(FechaPartido, "%d/%m/%Y") AS FechaPartido, GolesEquipo1, GolesEquipo2 FROM Partido p WHERE PartidoID = ?;';
  executeQuery(sql, [PartidoID], callback);
}

// Función específica para obtener los datos de la tabla "Partido" y "EquipoPartido"
function obtenerEquipo1Partido(PartidoID, callback) {
  const sql = 'SELECT Nombre AS Nombre1, Apellido AS Apellido1, Apodo AS Apodo1 FROM Partido p NATURAL JOIN EquipoPartido ep NATURAL JOIN Jugador j WHERE Equipo = 1 AND PartidoID = ?;';
  executeQuery(sql, [PartidoID], callback);
}

// Función específica para obtener los datos de la tabla "Partido" y "EquipoPartido"
function obtenerEquipo2Partido(PartidoID, callback) {
  const sql = 'SELECT Nombre AS Nombre2, Apellido AS Apellido2, Apodo AS Apodo2 FROM Partido p NATURAL JOIN EquipoPartido ep NATURAL JOIN Jugador j WHERE Equipo = 2 AND PartidoID = ?;';
  executeQuery(sql, [PartidoID], callback);
}

// Función específica para obtener la cantidad de partidos ganados, empatados y perdidos
function obtenerPartidosGEP(JugadorID, callback) {
  const sql = 'SELECT SUM(CASE WHEN (GolesEquipo1 > GolesEquipo2 AND Equipo = 1) OR (GolesEquipo2 > GolesEquipo1 AND Equipo = 2) THEN 1 ELSE 0 END) AS PartidosGanados, SUM(CASE WHEN GolesEquipo1 = GolesEquipo2 THEN 1 ELSE 0 END) AS PartidosEmpatados, SUM(CASE WHEN (GolesEquipo1 < GolesEquipo2 AND Equipo = 1) OR (GolesEquipo2 < GolesEquipo1 AND Equipo = 2) THEN 1 ELSE 0 END) AS PartidosPerdidos FROM EquipoPartido JOIN Partido ON EquipoPartido.PartidoID = Partido.PartidoID WHERE JugadorID = ? GROUP BY JugadorID;';
  executeQuery(sql, [JugadorID], callback);
}

module.exports = {
  executeQuery,
  obtenerJugadores,
  obtenerJugadoresyAvatares,
  obtenerJugadoresPartido,
  obtenerJugador,
  obtenerPartidos,
  obtenerPartido,
  obtenerEquipo1Partido,
  obtenerEquipo2Partido,
  obtenerPartidosGEP,
  db,
};
