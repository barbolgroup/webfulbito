const htmlEquipos = document.getElementById('htmlEquipos');
// Recuperar los JugadorID seleccionados del almacenamiento local
const jugadoresSeleccionadosJSON = localStorage.getItem('jugadoresSeleccionados');

// Comprobar si jugadoresSeleccionadosJSON no es nulo
if (jugadoresSeleccionadosJSON) {
  const jugadoresSeleccionados = JSON.parse(jugadoresSeleccionadosJSON);

  // Ahora puedes usar jugadoresSeleccionados en js/equiposArmados.js
  console.log('JugadorID seleccionados:', jugadoresSeleccionados);

  // Realizar una solicitud POST al servidor para enviar los JugadorID seleccionados
  fetch('/equiposArmadosSrv/seleccionados', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ jugadoresSeleccionados }),
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      // Continuar con el código existente para mostrar los equipos sugeridos
    })
    .catch((error) => {
      console.error('Error al enviar los JugadorID seleccionados:', error);
    });
} else {
  console.log('No se encontraron JugadorID seleccionados en el almacenamiento local.');
}


fetch('/equiposArmadosSrv')
  .then(response => response.json())
  .then(data => {
    console.log(data);
    // Obtener los datos de los equipos y promedios del objeto JSON
    const parsedData = JSON.parse(data);
    const equipo1 = parsedData.Equipo1;
    console.log(equipo1);
    const equipo2 = parsedData.Equipo2;
    console.log(equipo2);

    // Crear las filas HTML para mostrar los jugadores del Equipo 1
    const rowsEquipo1 = equipo1.Jugadores.map(jugador => `
      <tr>
        <td>${jugador.Jugador.Nombre} ${jugador.Jugador.Apellido} ${jugador.Jugador.Apodo ? '(' + jugador.Jugador.Apodo + ')' : ''}</td>
        <td>${(Math.min(jugador.Valoracion, 100)).toFixed(2)}</td>
      </tr>
    `);

    // Combina las cadenas en una sola cadena de texto sin comas
    const htmlEquipo1 = rowsEquipo1.join('');

    // Crear las filas HTML para mostrar los jugadores del Equipo 2
    const rowsEquipo2 = equipo2.Jugadores.map(jugador => `
      <tr>
        <td>${jugador.Jugador.Nombre} ${jugador.Jugador.Apellido} ${jugador.Jugador.Apodo ? '(' + jugador.Jugador.Apodo + ')' : ''}</td>
        <td>${(Math.min(jugador.Valoracion, 100)).toFixed(2)}</td>
      </tr>
    `);

    // Combina las cadenas en una sola cadena de texto sin comas
    const htmlEquipo2 = rowsEquipo2.join('');


    // Crear el HTML completo con las tablas de jugadores y promedios
    const html = `
        <div class="grilla-EquiposArmados">
					<div class="cabecera-EquiposArmados"> 
						<h2 id="titulo-EquiposArmados">Equipos sugeridos por la IA</h2>
					</div>
          <br>
          <br>
					<div class="columnaIzquierda-EquiposArmados"> 
						<table class="blueTable">
							<thead>
							  <tr>
							    <th>Equipo 1</th>
                  <th> ${(Math.min(equipo1.Promedio, 100)).toFixed(2)}</th>
							  </tr>
							</thead>
							<tbody>
							  ${htmlEquipo1}
              </tbody>
						</table>
					</div>
					<div class="columnaDerecha-EquiposArmados"> 
						<table class="blueTable">
							<thead>
							  <tr>
							    <th>Equipo 2</th>
                  <th> ${(Math.min(equipo2.Promedio, 100)).toFixed(2)}</th>
							  </tr>
							</thead>
							<tbody>
							  ${htmlEquipo2}
              </tbody>
						</table>
					</div>
				</div>
    `;

    // Agregar el HTML generado al elemento con ID 'htmlEquipos'
    htmlEquipos.innerHTML = html;
  });
