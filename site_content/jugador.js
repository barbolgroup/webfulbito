const express = require('express');
const db = require('./mariaDB.js'); // Importa el modelo de la base de datos

const router = express.Router();

// Define una ruta que espera un parámetro en la URL llamado "jugadorID"
router.get('/:jugadorID', (req, res) => {
  const jugadorID = req.params.jugadorID;
  console.log('JugadorID es: ', jugadorID);

  //defino variable para guardar resultado de las 2 consultas
  let resultadoCombinado = {}
  db.obtenerJugador(jugadorID, (err, result) => {
    if (err) {
      console.error('Error al obtener el jugador:', err);
      res.status(500).json({ error: 'Error al obtener el jugador' });
    } else {

      resultadoCombinado = result;
      console.log(result);
      
      db.obtenerPartidosGEP(jugadorID, (err, result) => {
        if (err) {
          console.error('Error al obtener el jugador:', err);
          res.status(500).json({ error: 'Error al obtener el jugador' });
        } else {
          resultadoCombinado = { ...resultadoCombinado[0], ...result[0] };
          resultadoCombinado = [ resultadoCombinado ];
          console.log(resultadoCombinado);

          res.json(resultadoCombinado);
        }
      });
      //res.json(result);
    }
  });

});

module.exports = router;
