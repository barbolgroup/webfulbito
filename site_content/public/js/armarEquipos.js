const htmlEquipos = document.getElementById('htmlElegirJugadores');
const numJugadores = 14; // Cantidad de jugadores que deseas seleccionar

// Array para almacenar los jugadores seleccionados
const jugadoresSeleccionados = Array(numJugadores).fill(null);

// Función para crear un menú desplegable
function createDropdown(playerList, index) {
  const dropdown = document.createElement('select');
  dropdown.innerHTML = '<option value="">Seleccionar jugador</option>';

  const jugadoresDisponibles = playerList.filter((player) => {
    return !jugadoresSeleccionados.includes(player.JugadorID);
  });

  jugadoresDisponibles.forEach((player) => {
    const option = document.createElement('option');
    const nombreCompleto = `${player.Nombre} ${player.Apellido}`;
    console.log(player.RecetaHTML);
    if (player.Apodo !== null) {
      option.textContent = `${nombreCompleto} (${player.Apodo})`;
    } else {
      option.textContent = nombreCompleto;
    }
    option.value = player.JugadorID;
    dropdown.appendChild(option);
  });

  // Asignar un evento change específico para este dropdown
  dropdown.addEventListener('change', (event) => {
    console.log('Dropdown change event triggered');
    const jugadorID = parseInt(event.target.value);
    console.log('Selected jugadorID:', jugadorID);

    if (jugadorID) {
      jugadoresSeleccionados[index] = jugadorID;
    } else {
      jugadoresSeleccionados[index] = null;
    }
    enableArmarEquiposButton();
  });

  return dropdown;
}

// Función para habilitar o deshabilitar el botón "Armar Equipos"
function enableArmarEquiposButton() {
  if (jugadoresSeleccionados.every((jugadorID) => jugadorID !== null)) {
    document.getElementById('armarEquiposButton').disabled = false;
  } else {
    document.getElementById('armarEquiposButton').disabled = true;
  }
}

// Función para redirigir a equiposArmados.html con los JugadorID seleccionados
function redirigirAEquiposArmados() {
  const jugadoresSeleccionadosJSON = JSON.stringify(jugadoresSeleccionados);
  localStorage.setItem('jugadoresSeleccionados', jugadoresSeleccionadosJSON);
  window.location.href = 'equiposArmados.html';
}

fetch('/armarEquiposSrv')
  .then((response) => response.json())
  .then((data) => {
    console.log(data);

    // Crear los menús desplegables y texto antes de cada uno
    for (let i = 0; i < numJugadores; i++) {
      // Crear un div que contenga la imagen y el texto
      const containerDiv = document.createElement('div');

      // Agregar el código HTML de la imagen antes del texto
      const avatarDiv = document.createElement('div');
      avatarDiv.className = 'Avatar';
      avatarDiv.innerHTML = `
      <div id="faceCard" class="faceCard">
        <img src="images/Avatar/background/bg_blue.png" style="box-shadow:grey 0px 0px 6px;left:5px;top:5px;" alt="">
      </div>
      `;
      containerDiv.appendChild(avatarDiv);

      // Agregar texto después de la imagen
      const texto = document.createElement('h3');
      texto.textContent = `Jugador ${i + 1}:`;
      containerDiv.appendChild(texto);

      // Agregar un salto de línea (enter visual)
      const lineBreak = document.createElement('br');
      containerDiv.appendChild(lineBreak);

      // Crear el dropdown y agregarlo al contenedor
      const dropdown = createDropdown(data, i);
      containerDiv.appendChild(dropdown);

      // Agregar el contenedor al div con el id 'htmlEquipos'
      htmlEquipos.appendChild(containerDiv);

      // Agregar un evento change al div contenedor para cambiar la imagen
      containerDiv.addEventListener('change', (event) => {
        if (event.target.tagName === 'SELECT') {
          const jugadorID = parseInt(event.target.value);
          const avatarDiv = containerDiv.querySelector('.faceCard'); // Obtener el div con clase 'faceCard' dentro del contenedor

          if (jugadorID) {
            const player = data.find((player) => player.JugadorID === jugadorID);
            if (player && player.RecetaHTML) {
              avatarDiv.innerHTML = player.RecetaHTML; // Actualizar el contenido HTML
            }
            jugadoresSeleccionados[i] = jugadorID;
          } else {
            avatarDiv.innerHTML = ''; // Limpiar el contenido si no se selecciona ningún jugador
            jugadoresSeleccionados[i] = null;
          }
          enableArmarEquiposButton();
        }
      });
    }
    // Agregar el botón "Armar Equipos"
    const armarEquiposButton = document.createElement('button');
    armarEquiposButton.id = 'armarEquiposButton';
    armarEquiposButton.textContent = 'Armar Equipos';
    armarEquiposButton.disabled = true; // Inicialmente deshabilitado
    armarEquiposButton.addEventListener('click', () => {
      redirigirAEquiposArmados(); // Llama a la función para redirigir con los JugadorID
    });
    htmlEquipos.appendChild(armarEquiposButton);
  });
