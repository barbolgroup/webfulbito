#!/bin/bash

#[by fleiva]


#Variables de colores
NC='\033[0m' 		# No Color
IYellow='\033[0;93m'    # Yellow
Green='\033[0;32m'      # Green

#Variable de nombre de host
HOST=`hostname`

# Variables para SOURCE_FILE y WORKDIRECTORY basadas en el hostname
case $HOST in
    CYCLOPS)
        SOURCE_FILE="/mnt/c/Users/fer_l/AppData/Roaming/DBeaverData/workspace6/General/Scripts/Script.sql"
        WORKDIRECTORY="$HOME/webfulbito"
        ;;
    NB1245)
        SOURCE_FILE="/mnt/c/Users/leiva.fernando/AppData/Roaming/DBeaverData/workspace6/General/Scripts/Script.sql"
        WORKDIRECTORY="$HOME/git_repos/webfulbito/"
        ;;
    *)
        echo "No hay rutas definidas para este host"
        exit
        ;;
esac

echo "◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻"
echo -e "${IYellow}🔹 HOST: ${Green}$HOST$NC"
echo -e "${IYellow}🔹 Directorio de trabajo: ${Green}$WORKDIRECTORY$NC"
echo -e "${IYellow}🔹 Archivo de DBeaver: ${Green}$SOURCE_FILE$NC"

cd $WORKDIRECTORY

DESTINATION="."

# Mostrar menú al usuario
echo "◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻◻"
echo ""
echo ""
echo "¿Qué deseas hacer?"
echo "1) Copiar de DBeaver a Docker 🦦 ➡  🐳"
echo "2) Copiar de Docker a DBeaver 🐳 ➡  🦦"
read -p "Selecciona una opción (1 o 2): " OPTION
echo ""

case $OPTION in
    1)
        if [ -f "$SOURCE_FILE" ]; then
            cp "$SOURCE_FILE" "$DESTINATION"
            echo "✅ El archivo ha sido copiado con éxito a $DESTINATION."
        else
            echo "❌ El archivo $SOURCE_FILE no existe."
        fi
        ;;
    2)
        if [ -f "$DESTINATION/$(basename $SOURCE_FILE)" ]; then
            cp "$DESTINATION/$(basename $SOURCE_FILE)" "$SOURCE_FILE"
            echo "✅ El archivo ha sido copiado con éxito a $SOURCE_FILE."
        else
            echo "❌ El archivo $DESTINATION/$(basename $SOURCE_FILE) no existe."
        fi
        ;;
    *)
        echo "❌ Opción no válida."
        ;;
esac
echo ""
