const express = require('express');
const db = require('./mariaDB.js'); // Importa el modelo de la base de datos
//const db = require('./postgreSQL.js'); // Importa el modelo de la base de datos
const router = express.Router();
const bodyParser = require('body-parser')
router.use(express.urlencoded({ extended: true }));
router.use(bodyParser.json());


function crearArregloJugadorValoracion(datos) { //Toma el json con toda la info de los jugadores y arma un array de 14 elementos, donde cada elemento es un [jugadorID, Valoracion]
  return datos.map((fila) => [fila.JugadorID, fila.Valoracion]);
}


function dividirEnEquiposDraftSerpentina(arr) { //separa el arreglo de 14 jugadores en dos arreglos de 7 jugadores intercalándolos por sus valoraciones
  const equipo1 = [];
  const equipo2 = [];

  for (let i = 0; i < arr.length; i++) {
    if (i % 2 === 0) {
      equipo1.push(arr[i]);
    } else {
      equipo2.push(arr[i]);
    }
  }

  return [equipo1, equipo2];
}
function calcularPromedio(arr) {
  if (arr.length === 0) {
    return 0; // Manejo de caso vacío para evitar división por cero.
  }

  let suma = 0;
  for (let i = 0; i < arr.length; i++) {
    suma += arr[i][1]; // Suma el segundo elemento de cada subarreglo.
  }

  const promedio = suma / arr.length;
  return promedio;
}

function encontrarMayor(constante1, constante2) { //Me indica cual es el equipo que tiene mejor promedio
  if (constante1 > constante2) {
    return constante1;
  } else if (constante2 > constante1) {
    return constante2;
  } else {
    return "Ambas constantes son iguales";
  }
}

function diferenciaPromedios(constante1, constante2) { //Me calcula la diferencia entre los promedios
  if (constante1 > constante2) {
    return constante1 - constante2;
  } else {
    return constante2 - constante1;
  }
}

function intercambiarElementosEntreArrays(posicionArray1, posicionArray2, equipo1, equipo2) { //intercambia 2 jugadores entre los equipos
  // Verificar si las posiciones son válidas
  if (
    posicionArray1 < 0 ||
    posicionArray1 >= equipo1.length ||
    posicionArray2 < 0 ||
    posicionArray2 >= equipo2.length
  ) {
    console.log("Posiciones inválidas");
    return;
  }

  // Realizar el intercambio de elementos
  const elementoIntercambio = equipo1[posicionArray1];
  equipo1[posicionArray1] = equipo2[posicionArray2];
  equipo2[posicionArray2] = elementoIntercambio;
}

function compararEIntercambiar(equipo1, equipo2) {
  let huboIntercambio = true; // Variable para controlar si se realizó un intercambio

  //calculo promedio de cada equipo.
  let promedioEquipo1 = calcularPromedio(equipo1);
  let promedioEquipo2 = calcularPromedio(equipo2);
  console.log("Promedio Equipo 1:", promedioEquipo1);
  console.log("Promedio Equipo 2:", promedioEquipo2);
  let iteracion = 1;
  console.log("ITERACION ", iteracion);
  while (huboIntercambio) {
    // Reiniciar la variable en cada iteración del bucle
    huboIntercambio = false;

    // Obtengo la diferencia entre los promedios
    let promEquipo1 = calcularPromedio(equipo1);
    let promEquipo2 = calcularPromedio(equipo2);
    const difProm = diferenciaPromedios(promEquipo1, promEquipo2);
    console.log("La diferencia de promedios es: ", difProm);

    // comienzo proceso de intercambio de jugadores


    for (let i = 0; i < equipo1.length; i++) {
      const valorEquipo1 = equipo1[i][1];
      for (let j = 0; j < equipo2.length; j++) {
        const valorEquipo2 = equipo2[j][1];
        if (valorEquipo1 > valorEquipo2) {
          if ((valorEquipo1 - valorEquipo2) < difProm) {
            intercambiarElementosEntreArrays(i, j, equipo1, equipo2);
            console.log("Intercambié el jugador ", i, "del equipo 1 por el jugador ", j, "del equipo 2.");
            huboIntercambio = true; // Se realizó un intercambio, marcarlo como true
          }
        }
      }
    }
    iteracion++;
    console.log(" ");

    if (huboIntercambio) {
      console.log("ITERACION ", iteracion);
    }
  }
}

//FIN DECLARACIÓN DE FUNCIONES
//INICIO PROGRAMA

console.log('-------------------------------------');


router.post('/seleccionados', (req, res) => {
  const { jugadoresSeleccionados } = req.body;
  console.log('JugadorID seleccionados en el servidor:', jugadoresSeleccionados);
  // Realiza cualquier procesamiento necesario aquí
  jugadorIDs = jugadoresSeleccionados;
  // Luego, envía una respuesta JSON
  res.json({ message: 'JugadorID seleccionados recibidos con éxito en el servidor' });
});

let jugadorIDs = [1, 2, 3, 4, 5, 6, 11, 12, 13, 14, 15, 16, 21, 31];
// Ruta para obtener los datos de la tabla "Jugador"
router.get('/', (req, res) => {
  db.obtenerJugadoresPartido(jugadorIDs, (err, result) => {
    if (err) {
      console.error('Error al obtener los datos de la tabla "Jugador":', err);
      res.status(500).json({ error: 'Error al obtener los datos de la tabla "Jugador"' });
    } else {
      //Acá va toda la lógica para armar los equipos
      console.log(result); //Muestra lo que trajo obtenerJugadoresPartido de la BD
      const arrayConvocados = crearArregloJugadorValoracion(result);
      console.log("Los convocados son: ", arrayConvocados);
      const [equipo1, equipo2] = dividirEnEquiposDraftSerpentina(arrayConvocados);
      console.log("Equipo 1:", equipo1);
      console.log("Equipo 2:", equipo2);



      compararEIntercambiar(equipo1, equipo2);
      //calculo promedio de cada equipo luego de los intercambios
      const promedioEquipo1 = calcularPromedio(equipo1);
      const promedioEquipo2 = calcularPromedio(equipo2);
      console.log("Luego de los intercambios");
      console.log("Equipo 1:", equipo1);
      console.log("Equipo 2:", equipo2);
      console.log("Promedio Equipo 1:", promedioEquipo1);
      console.log("Promedio Equipo 2:", promedioEquipo2);
      //Obtengo la diferencia entre los promedios
      const diferenciaProm = diferenciaPromedios(promedioEquipo1, promedioEquipo2);
      console.log("La diferencia de promedios es: ", diferenciaProm);



      //Generado por chatGPT para armar el JSON que le voy a pasar al front
      function obtenerDatosJugador(jugadorID) {
        // Buscar el jugador por jugadorID en los datos de result
        const jugador = result.find((row) => row.JugadorID === jugadorID);
        return jugador;
      }

      const equipo1ConDatos = equipo1.map(([jugadorID, valoracion]) => ({
        Jugador: obtenerDatosJugador(jugadorID),
        Valoracion: valoracion
      }));

      const equipo2ConDatos = equipo2.map(([jugadorID, valoracion]) => ({
        Jugador: obtenerDatosJugador(jugadorID),
        Valoracion: valoracion
      }));

      const resultadoJSON = {
        Equipo1: {
          Jugadores: equipo1ConDatos,
          Promedio: promedioEquipo1
        },
        Equipo2: {
          Jugadores: equipo2ConDatos,
          Promedio: promedioEquipo2
        }
      };

      //console.log(JSON.stringify(resultadoJSON, null, 2));
      megaJSON = JSON.stringify(resultadoJSON, null, 2);
      res.json(megaJSON);
    }
  });
});

module.exports = router;