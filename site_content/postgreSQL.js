const { Pool } = require('pg');

// Configuración de la conexión a la base de datos PostgreSQL
const db = new Pool({
  user: 'fl0user',         // Cambia esto por tu nombre de usuario de PostgreSQL
  password: 'XcHVZJNjD14g',   // Cambia esto por tu contraseña de PostgreSQL
  host: 'ep-bold-sound-34491744.ap-southeast-1.aws.neon.tech',           // Cambia esto si tu base de datos está en otro host
  database: 'Fulbitopostgres',  // Cambia esto por el nombre real de tu base de datos
  port: 5432,   // Cambia esto si tu base de datos está en un puerto diferente
  ssl: true,               
});

// Conexión a la base de datos
db.connect((err) => {
  if (err) {
    console.error('Error al conectar a la base de datos:', err);
  } else {
    console.log('Conexión exitosa a la base de datos PostgreSQL');
  }
});

// Función genérica para ejecutar consultas SQL
function executeQuery(sql, values, callback) {
  db.query(sql, values, (err, result) => {
    if (err) {
      console.error('Error al ejecutar la consulta:', err);
      callback(err, null);
    } else {
        //console.log(result);
      callback(null, result);
    }
  });
}

// Función específica para obtener los datos de la tabla "Jugador"
function obtenerJugadores(callback) {
    console.log("Ejecuto obtenerJugadores");
  const sql = 'SELECT JugadorID, Nombre, Apellido, Apodo, SuspensionesTotales, PartidosJugados, Valoracion FROM Jugador ORDER BY Valoracion DESC;';
  executeQuery(sql, [], callback);
}

// Función específica para obtener los datos de la tabla "Jugador" y "Avatar"
function obtenerJugador(JugadorID, callback) {
    console.log("Ejecuto obtenerJugador");
  const sql = 'SELECT JugadorID, Nombre, Apellido, Apodo, SuspensionesTotales, PartidosJugados, Valoracion, RecetaHTML FROM Jugador NATURAL JOIN Avatar WHERE JugadorID = $1;';
  executeQuery(sql, [JugadorID], callback);
}

// Función específica para obtener los datos de la tabla "Partido"
function obtenerPartidos(callback) {
  console.log("Ejecuto obtenerPartidos");  
  const sql = 'SELECT PartidoID, TO_CHAR(FechaPartido, \'DD/MM/YYYY\') AS FechaPartido, GolesEquipo1, GolesEquipo2, PremioEquipo1, PremioEquipo2 FROM Partido ORDER BY PartidoID DESC;';
  executeQuery(sql, [], callback);
}

// Función específica para obtener los datos de la tabla "Partido" y "EquipoPartido"
function obtenerPartido(PartidoID, callback) {
    console.log("Ejecuto obtenerPartido");  
  const sql = 'SELECT TO_CHAR(FechaPartido, \'DD/MM/YYYY\') AS FechaPartido, GolesEquipo1, GolesEquipo2 FROM Partido p WHERE PartidoID = $1;';
  executeQuery(sql, [PartidoID], callback);
}

// Función específica para obtener los datos de la tabla "Partido" y "EquipoPartido"
function obtenerEquipo1Partido(PartidoID, callback) {
    console.log("Ejecuto obtenerEquipo1Partido");  
  const sql = 'SELECT Nombre AS Nombre1, Apellido AS Apellido1, Apodo AS Apodo1 FROM Partido p NATURAL JOIN EquipoPartido ep NATURAL JOIN Jugador j WHERE Equipo = 1 AND PartidoID = $1;';
  executeQuery(sql, [PartidoID], callback);
}

// Función específica para obtener los datos de la tabla "Partido" y "EquipoPartido"
function obtenerEquipo2Partido(PartidoID, callback) {
    console.log("Ejecuto obtenerEquipo2Partido");  
  const sql = 'SELECT Nombre AS Nombre2, Apellido AS Apellido2, Apodo AS Apodo2 FROM Partido p NATURAL JOIN EquipoPartido ep NATURAL JOIN Jugador j WHERE Equipo = 2 AND PartidoID = $1;';
  executeQuery(sql, [PartidoID], callback);
}

module.exports = {
  executeQuery,
  obtenerJugadores,
  obtenerJugador,
  obtenerPartidos,
  obtenerPartido,
  obtenerEquipo1Partido,
  obtenerEquipo2Partido,
  db,
};
